//
//  FollowTableViewCell.h
//  Lynixt
//
//  Created by Rizwan on 8/29/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageViewProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblUsername;
@property (weak, nonatomic) IBOutlet UITextView *textViewDesc;
@property (weak, nonatomic) IBOutlet UIButton *btnFollow;


@end
