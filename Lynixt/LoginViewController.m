//
//  LoginViewController.m
//  Lynixt
//
//  Created by Rizwan on 7/22/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "LoginViewController.h"
#import "SideMenuViewController.h"

@interface LoginViewController () {
    
    __weak IBOutlet UITextField *textFieldEmail;
    __weak IBOutlet UITextField *textFieldPassword;
    __weak IBOutlet UIView *viewForgotPassword;
    __weak IBOutlet UITextField *textFieldForgotPassword;
    IBOutlet UITapGestureRecognizer *tapGesture;
    
    __weak IBOutlet UIView *viewEmail;
    __weak IBOutlet UIView *viewPassword;
    
    
    __weak IBOutlet UILabel *lblForgotPassword;
    __weak IBOutlet UILabel *lblEmail;
    Loader *loader;
    int yPos;
    NSDictionary *dicUser;
}

@property(nonatomic,strong) SWRevealViewController *swrevealviewcontroller;

- (IBAction)forgotPasswordBtnPressed:(id)sender;
- (IBAction)loginBtnPressed:(id)sender;
- (IBAction)sendBtnPressed:(id)sender;
- (IBAction)cancelBtnPressed:(id)sender;
- (IBAction)tapGestureTapped:(id)sender;

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    loader = [[Loader alloc] init];
    yPos = self.view.frame.origin.y;
    
    NSString *strEmail = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"user_data.email"];
    [textFieldEmail setText:strEmail];
    
    //[textFieldPassword setText:@"UGdV9hMs"];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    //[super preferredStatusBarStyle];
    return UIStatusBarStyleLightContent;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark --
#pragma mark -- UITextField Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [tapGesture setEnabled:NO];
    return [textField resignFirstResponder];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [tapGesture setEnabled:YES];
    if ([[UIScreen mainScreen] bounds].size.height == 480) {
        CGRect frame = self.view.frame;
        frame.origin.y = yPos-60;
        self.view.frame = frame;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if ([[UIScreen mainScreen] bounds].size.height == 480) {
        CGRect frame = self.view.frame;
        frame.origin.y = yPos;
        self.view.frame = frame;
    }
}

- (IBAction)forgotPasswordBtnPressed:(id)sender {
    
    [lblEmail setText:@"Email"];
    [lblForgotPassword setText:@"Reset Password"];
    [textFieldForgotPassword setPlaceholder:@"Username or Email"];

    [textFieldForgotPassword setText:@""];
    
    [UIView transitionWithView:viewForgotPassword duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [viewForgotPassword setAlpha:1.0];
    } completion:^(BOOL finished) {
        
    }];
}

- (IBAction)loginBtnPressed:(id)sender {
    
//    if (![self emailFormat:textFieldEmail.text])
    if (textFieldEmail.text.length != 0 && textFieldPassword.text.length != 0) {
        
        [loader startAnimatingWithViewController:self];
        AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
        
        NSDictionary *dic = @{@"email": textFieldEmail.text,
                              @"password": textFieldPassword.text};
        
        NSString *strUrlPath = [NSString stringWithFormat:@"%@/signIn", baseURLPath];
        
        [httpClient postPath:strUrlPath parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [loader stopAnimating];
            
            NSDictionary *dicJson = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
            dicUser = dicJson[@"finalData"];
            
            NSLog(@"data %@", dicUser);
            
            if ([dicJson[@"response"] isEqualToString:@"SUCCESS"]) {
                
                [self enterTotheApp];
//                if ([dicUser[@"verify_status"] boolValue]) {
//                    
//                    [self enterTotheApp];
//                }
//                else {
//                    [lblEmail setText:@"Code"];
//                    [lblForgotPassword setText:@"Authentication Code"];
//                    [textFieldForgotPassword setText:@""];
//                    [textFieldForgotPassword setPlaceholder:@""];
//                    [UIView transitionWithView:viewForgotPassword duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
//                        [viewForgotPassword setAlpha:1.0];
//                    } completion:^(BOOL finished) {
//                        
//                    }];
//                }
                
                [[NSUserDefaults standardUserDefaults] setObject:dicUser forKey:@"user_data"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
            }
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:dicJson[@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [loader stopAnimating];
        }];
    }
    else {
        if (textFieldEmail.text.length == 0) {
            [self shakeView:viewEmail];
        }
        if (textFieldPassword.text.length == 0) {
            [self shakeView:viewPassword];
        }
    }
}

- (IBAction)sendBtnPressed:(id)sender {
    
    if ([lblForgotPassword.text isEqualToString:@"Authentication Code"]) {
        if (textFieldForgotPassword.text.length == 0) {
            
            [self shakeView:textFieldForgotPassword];
            
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Field required" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
        }
        else {
            if ([dicUser[@"verify_code"] isEqualToString:textFieldForgotPassword.text]) {
                
                AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
                
                NSDictionary *dic = @{@"user_id": dicUser[@"user_id"],
                                      @"verify_code": textFieldForgotPassword.text};
                
                NSString *strUrlPath = [NSString stringWithFormat:@"%@/activateUserAccount", baseURLPath];
                
                [httpClient postPath:strUrlPath parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    [textFieldForgotPassword resignFirstResponder];
                    NSDictionary *dicJson = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
                    if ([dicJson[@"response"] isEqualToString:@"SUCCESS"]) {
                        
                        [UIView transitionWithView:viewForgotPassword duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                            [viewForgotPassword setAlpha:0.0];
                        } completion:^(BOOL finished) {
                            
                        }];
                    }
                    else {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:dicJson[@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                    }
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    [textFieldForgotPassword resignFirstResponder];
                }];

                [self enterTotheApp];
            }
            else {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Not a valid code" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
    }
    else {
        if (textFieldForgotPassword.text.length == 0) {
            
            [self shakeView:textFieldForgotPassword];
            
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Email address required" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//            [alert show];
        }
        else if (![self emailFormat:textFieldForgotPassword.text]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter a valid Email address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else {
            
            [loader startAnimatingWithViewController:self];
            AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
            
            NSDictionary *dic = @{@"email": textFieldForgotPassword.text};
            
            NSString *strUrlPath = [NSString stringWithFormat:@"%@/forgotPassword", baseURLPath];
            
            [httpClient postPath:strUrlPath parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                [loader stopAnimating];
                
                [textFieldForgotPassword resignFirstResponder];
                NSDictionary *dicJson = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
                if ([dicJson[@"status"] isEqualToString:@"TRUE"]) {
                    
                    [UIView transitionWithView:viewForgotPassword duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                        [viewForgotPassword setAlpha:0.0];
                    } completion:^(BOOL finished) {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:dicJson[@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        [self cancelBtnPressed:nil];
                    }];
                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:dicJson[@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [loader stopAnimating];
                [textFieldForgotPassword resignFirstResponder];
            }];
        }
    }
}

- (IBAction)cancelBtnPressed:(id)sender {
    
    [textFieldForgotPassword resignFirstResponder];
    [UIView transitionWithView:viewForgotPassword duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [viewForgotPassword setAlpha:0.0];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)enterTotheApp {
    
    SideMenuViewController *leftMenuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SideMenuViewController"];
    UITabBarController *templateMenuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
    
    
//	CGRect newRect = CGRectMake(rect.origin.x, rect.origin.y + 6, rect.size.width, rect.size.height); //fix UITabBarItem bug
    
    UITabBar *tabBar = templateMenuVC.tabBar;
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
    UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
    UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
    UITabBarItem *tabBarItem5 = [tabBar.items objectAtIndex:4];
    
    [tabBarItem1 setFinishedSelectedImage:[UIImage imageNamed:@"tab1-home-selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab1-home-normal.png"]];
    [tabBarItem2 setFinishedSelectedImage:[UIImage imageNamed:@"tab2-explore-selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab2-explore-normal.png"]];
    [tabBarItem3 setFinishedSelectedImage:[UIImage imageNamed:@"tab3-more-selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab3-more-normal.png"]];
    [tabBarItem4 setFinishedSelectedImage:[UIImage imageNamed:@"tab4-notification-selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab4-notification-normal.png"]];
    [tabBarItem5 setFinishedSelectedImage:[UIImage imageNamed:@"tab5-profile-selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab5-profile-normal.png"]];
    
    self.swrevealviewcontroller = [[SWRevealViewController alloc] initWithRearViewController:leftMenuVC frontViewController:templateMenuVC];
    [self.swrevealviewcontroller setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:self.swrevealviewcontroller animated:YES completion:nil];
    
    CGRect rect = templateMenuVC.tabBar.frame;
    //rect.origin.y += 16;
    CGRect newRect = CGRectMake(rect.origin.x, rect.origin.y + 10, rect.size.width, rect.size.height); //fix UITabBarItem bug
    templateMenuVC.tabBar.frame = newRect;

    if (!(floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)) {
        
        if ([[UIScreen mainScreen] bounds].size.height == 480) {

            [self hideTabBar:templateMenuVC withMargin:0];
        }
        else {
            [self hideTabBar:templateMenuVC withMargin:88];
        }
    }
    
}

// Method implementations
- (void)hideTabBar:(UITabBarController *) tabbarcontroller withMargin:(NSInteger )margin
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    
    for(UIView *view in tabbarcontroller.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]]) {
            [view setFrame:CGRectMake(view.frame.origin.x, 421+margin, view.frame.size.width, view.frame.size.height)];
        }
        else {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 421+margin)];
        }
    }
    
    [UIView commitAnimations];
}

- (IBAction)tapGestureTapped:(id)sender {
    [textFieldEmail resignFirstResponder];
    [textFieldPassword resignFirstResponder];
    [textFieldForgotPassword resignFirstResponder];
    [tapGesture setEnabled:NO];
}
@end
