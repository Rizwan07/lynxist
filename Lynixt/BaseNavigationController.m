//
//  BaseNavigationController.m
//  Lynixt
//
//  Created by Rizwan on 7/22/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "BaseNavigationController.h"

@interface BaseNavigationController ()

@end

@implementation BaseNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        
        [self.view setTintColor:[UIColor blackColor]];
        
    }
    else {
        [self.navigationBar setTintColor:[UIColor blackColor]];
        
        [[UINavigationBar appearance] setTitleTextAttributes: @{
                                                                UITextAttributeTextColor: [UIColor whiteColor],
                                                                UITextAttributeTextShadowColor: [UIColor clearColor],
                                                                UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)],
                                                                UITextAttributeFont: [UIFont fontWithName:@"Avenir-Light" size:20.0f]
                                                                }];
        
        
        //[self.navigationBar setBackgroundColor:[UIColor blackColor]];
        //[self.view setTintColor:[UIColor whiteColor]];
        // do stuff for older versions than iOS 7
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (UIStatusBarStyle)preferredStatusBarStyle {
    //[super preferredStatusBarStyle];
    return UIStatusBarStyleLightContent;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
