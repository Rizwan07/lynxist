//
//  AccountsViewController.m
//  Lynixt
//
//  Created by Rizwan on 8/28/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "AccountsViewController.h"
#import "HomeTableViewCell.h"
#import "FollowTableViewCell.h"
//#import "UIImageView+WebCache.h"

@interface AccountsViewController () <UIActionSheetDelegate> {
    
    __weak IBOutlet UIImageView *imageViewProfile;
    __weak IBOutlet UILabel *lblFullName;
    __weak IBOutlet UILabel *lblUserName;
    __weak IBOutlet UILabel *lblDesc;
    __weak IBOutlet UIButton *btnPosts;
    __weak IBOutlet UIButton *btnFollowing;
    __weak IBOutlet UIButton *btnFollowers;
    __weak IBOutlet UITableView *_tableView;
    
    NSArray *arrAccountData;
}

- (IBAction)menuBtnsPressed:(id)sender;
- (IBAction)profileBtnPressed:(id)sender;

@end

@implementation AccountsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [lblFullName setText:[defaults valueForKeyPath:@"user_data.full_name"]];
    [lblUserName setText:[NSString stringWithFormat:@"/%@", [defaults valueForKeyPath:@"user_data.user_name"]]];
    [lblDesc setText:[defaults valueForKeyPath:@"user_data.description"]];
    
    if ([defaults valueForKeyPath:@"user_data.profile_pic"] != nil) {
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[defaults valueForKeyPath:@"user_data.profile_pic"]]];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            if (data && !connectionError) {
                [imageViewProfile setImage:[UIImage imageWithData:data]];
            }
        }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getDataFromUrlPath:(NSString *)strPath {

    //[loader startAnimatingWithViewController:self];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
    
    NSDictionary *dic = @{@"user_id": [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"user_data.user_id"]};
    
    NSString *strUrlPath = [NSString stringWithFormat:@"%@/%@", baseURLPath, strPath];
    
    [httpClient postPath:strUrlPath parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
//        [loader stopAnimating];
        
        NSDictionary *dicJson = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
        
        if ([dicJson[@"response"] isEqualToString:@"SUCCESS"]) {
            
            arrAccountData = dicJson[@"finalData"];
            [_tableView reloadData];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:dicJson[@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //[loader stopAnimating];
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [btnPosts isSelected] ? 310 : 104;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrAccountData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dic = arrAccountData[indexPath.row];
    
    if ([btnPosts isSelected]) {
        
        HomeTableViewCell *cell = (HomeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell1" forIndexPath:indexPath];
        return cell;
    }
    else {
        FollowTableViewCell *cell = (FollowTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell2" forIndexPath:indexPath];
        [cell.imageViewProfile sd_setImageWithURL:dic[@"profile_pic"]];
        [cell.lblName setText:dic[@"full_name"]];
        [cell.lblUsername setText:[NSString stringWithFormat:@"/%@", dic[@"user_name"]]];
        [cell.textViewDesc setText:dic[@"description"]];
        [cell.btnFollow addTarget:self action:@selector(followBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell setTag:indexPath.row];
        return cell;
    }
}

- (void)followBtnPressed:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    [btn setSelected:![btn isSelected]];
    //user_id=110&other_user_id=152&service_type=add
    //[loader startAnimatingWithViewController:self];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
    
    NSDictionary *dic = @{@"user_id": [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"user_data.user_id"],
                          @"other_user_id": arrAccountData[btn.tag][@"user_id"],
                          @"service_type": (btn.isSelected ? @"add" : @"delete")};
    
    NSString *strUrlPath = [NSString stringWithFormat:@"%@/addDeleteFollow", baseURLPath];
    
    [httpClient postPath:strUrlPath parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        //        [loader stopAnimating];
        
        NSDictionary *dicJson = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
        
        if ([dicJson[@"response"] isEqualToString:@"SUCCESS"]) {
            
            arrAccountData = dicJson[@"finalData"];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:dicJson[@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //[loader stopAnimating];
    }];
    
}

- (IBAction)menuBtnsPressed:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    switch (btn.tag) {
        case 0:
            [self getDataFromUrlPath:@""];
            break;
        case 1:
            [self getDataFromUrlPath:@"following"];
            break;
        case 2:
            [self getDataFromUrlPath:@"followers"];
            break;
        default:
            break;
    }
    
    [btnPosts setSelected:btn.tag == 0];
    [btnFollowing setSelected:btn.tag == 1];
    [btnFollowers setSelected:btn.tag == 2];
    
    [_tableView reloadData];
}

- (IBAction)profileBtnPressed:(id)sender {
    
//    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo", @"Photo Gallery", nil];
//    [actionSheet showFromTabBar:self.tabBarController.tabBar];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        NSLog(@"take photo");
    }
    if (buttonIndex == 1) {
        NSLog(@"photo gallery");
    }
}

@end
