//
//  HomeViewController.m
//  Lynixt
//
//  Created by Rizwan on 7/22/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "HomeViewController.h"
#import "HomeTableViewCell.h"

@interface HomeViewController () {
    
    __weak IBOutlet UITableView *_tableView;
    __weak IBOutlet UIButton *btnAll;
    __weak IBOutlet UIButton *btnFriends;
    __weak IBOutlet UIButton *btnNewsBlogs;
    __weak IBOutlet UIButton *btnTopLinks;
}

- (IBAction)homeMenuBtnsPressed:(id)sender;

- (IBAction)flagBtnPressed:(id)sender forEvent:(UIEvent *)event;
- (IBAction)commentBtnPressed:(id)sender forEvent:(UIEvent *)event;
- (IBAction)roundBtnPressed:(id)sender forEvent:(UIEvent *)event;
- (IBAction)shareBtnPressed:(id)sender forEvent:(UIEvent *)event;
@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    //[super preferredStatusBarStyle];
    return UIStatusBarStyleLightContent;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)homeMenuBtnsPressed:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    
    switch (btn.tag) {
        case 0:
            
            break;
        case 1:
            
            break;
        case 2:
            
            break;
        case 3:
            
            break;
            
        default:
            break;
    }
    
    [btnAll         setSelected:btn.tag == 0];
    [btnFriends     setSelected:btn.tag == 1];
    [btnNewsBlogs   setSelected:btn.tag == 2];
    [btnTopLinks    setSelected:btn.tag == 3];
}

- (NSIndexPath *)getIndexPathForEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint point = [touch locationInView:_tableView];
    return [_tableView indexPathForRowAtPoint:point];
}

- (IBAction)flagBtnPressed:(id)sender forEvent:(UIEvent *)event {
    
    NSIndexPath *indexPath = [self getIndexPathForEvent:event];
    NSLog(@"index Path %@", indexPath);
}

- (IBAction)commentBtnPressed:(id)sender forEvent:(UIEvent *)event {
    
    NSIndexPath *indexPath = [self getIndexPathForEvent:event];
    NSLog(@"index Path %@", indexPath);
}

- (IBAction)roundBtnPressed:(id)sender forEvent:(UIEvent *)event {
    
    NSIndexPath *indexPath = [self getIndexPathForEvent:event];
    NSLog(@"index Path %@", indexPath);
}

- (IBAction)shareBtnPressed:(id)sender forEvent:(UIEvent *)event {
    
    NSIndexPath *indexPath = [self getIndexPathForEvent:event];
    NSLog(@"index Path %@", indexPath);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    HomeTableViewCell *cell = (HomeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    
    
    return cell;
}


@end
