//
//  Loader.h
//  NursePal
//
//  Created by Rizwan on 3/4/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Loader : NSObject

- (void)startAnimatingWithViewController:(UIViewController *)vc;
- (void)stopAnimating;

@end
