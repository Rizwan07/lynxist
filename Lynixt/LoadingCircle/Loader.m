//
//  Loader.m
//  NursePal
//
//  Created by Rizwan on 3/4/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "Loader.h"
#import "LoadingCircle.h"

@interface Loader () {
    LoadingCircle *loadingCircle;
}

@end

@implementation Loader


- (void)startAnimatingWithViewController:(UIViewController *)vc {
    if (!loadingCircle) {
        loadingCircle = [[LoadingCircle alloc]initWithNibName:nil bundle:nil];
        loadingCircle.view.backgroundColor = [UIColor clearColor];
        
        //Colors for layers
        //loadingCircle.colorLoadingLayer = [UIColor colorWithRed:218.0/255.0 green:139.0/255.0 blue:38.0/255.0 alpha:1];//[UIColor colorWithRed:0 green:0.4 blue:0 alpha:0.4];
        loadingCircle.colorLoadingLayer = [UIColor clearColor];
        int size = 100;
        
        CGRect frame = loadingCircle.view.frame;
        frame.size.width = size ;
        frame.size.height = size;
        frame.origin.x = vc.view.frame.size.width / 2 - frame.size.width / 2;
        frame.origin.y = vc.view.frame.size.height / 2 - frame.size.height / 2;
        loadingCircle.view.frame = frame;
        loadingCircle.view.layer.zPosition = MAXFLOAT;
    }
    [vc.view addSubview: loadingCircle.view];
    
}

- (void)stopAnimating {
    if (loadingCircle) {
        
        [loadingCircle.view removeFromSuperview];
    }
}

@end
