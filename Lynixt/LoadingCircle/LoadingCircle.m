//
//  MZLoadingCircle.m
//  MZLoading
//
//  Created by Serghei Mazur on 10/31/13.
//  Copyright (c) 2013 Serghei Mazur. All rights reserved.
//

#import "LoadingCircle.h"

@interface LoadingLayer : CALayer {
}

@property (assign) CGRect ovalRect;
@property (assign) int lineWidth;
@property (nonatomic) UIColor *colorLine;

@end

@implementation LoadingLayer

@synthesize ovalRect,lineWidth,colorLine;


- (void)drawInContext:(CGContextRef)theContext
{
    
    CGPoint point = CGPointMake(CGRectGetMidX(ovalRect), CGRectGetMidY(ovalRect));
    CGFloat radius = CGRectGetWidth(ovalRect) / 2;
    
    //// Oval 1 Drawing
    UIBezierPath* oval1PathBig = [UIBezierPath bezierPath];
    [oval1PathBig addArcWithCenter: point radius: radius startAngle: 195 * M_PI/180 endAngle: 220 * M_PI/180 clockwise: YES];
    
    CGContextAddPath(theContext, oval1PathBig.CGPath);
    
    UIBezierPath* oval1PathSmall = [UIBezierPath bezierPath];
    [oval1PathSmall addArcWithCenter: point radius: radius startAngle: 245 * M_PI/180 endAngle: 255 * M_PI/180 clockwise: YES];
    
    CGContextAddPath(theContext, oval1PathSmall.CGPath);

    //// Oval 2 Drawing
    UIBezierPath* oval2PathBig = [UIBezierPath bezierPath];
    [oval2PathBig addArcWithCenter: point radius: radius startAngle: 285 * M_PI/180 endAngle: 310 * M_PI/180 clockwise: YES];
    
    CGContextAddPath(theContext, oval2PathBig.CGPath);
    
    UIBezierPath* oval2PathSmall = [UIBezierPath bezierPath];
    [oval2PathSmall addArcWithCenter: point radius: radius startAngle: 335 * M_PI/180 endAngle: 345 * M_PI/180 clockwise: YES];
    
    CGContextAddPath(theContext, oval2PathSmall.CGPath);
    
    //// Oval 3 Drawing
    UIBezierPath* oval3PathBig = [UIBezierPath bezierPath];
    [oval3PathBig addArcWithCenter: point radius: radius startAngle: 15 * M_PI/180 endAngle: 40 * M_PI/180 clockwise: YES];
    
    CGContextAddPath(theContext, oval3PathBig.CGPath);
    
    UIBezierPath* oval3PathSmall = [UIBezierPath bezierPath];
    [oval3PathSmall addArcWithCenter: point radius: radius startAngle: 65 * M_PI/180 endAngle: 75 * M_PI/180 clockwise: YES];
    
    CGContextAddPath(theContext, oval3PathSmall.CGPath);
    
    //// Oval 4 Drawing
    UIBezierPath* oval4PathBig = [UIBezierPath bezierPath];
    [oval4PathBig addArcWithCenter: point radius: radius startAngle: 105 * M_PI/180 endAngle: 130 * M_PI/180 clockwise: YES];
    
    CGContextAddPath(theContext, oval4PathBig.CGPath);
    
    UIBezierPath* oval4PathSmall = [UIBezierPath bezierPath];
    [oval4PathSmall addArcWithCenter: point radius: radius startAngle: 155 * M_PI/180 endAngle: 165 * M_PI/180 clockwise: YES];
    
    CGContextAddPath(theContext, oval4PathSmall.CGPath);
    
    CGContextSetLineWidth(theContext,lineWidth);
    CGContextSetStrokeColorWithColor(theContext, colorLine.CGColor);
    CGContextStrokePath(theContext);
}

@end

@implementation LoadingCircle

- (void)loadView {
    UIView *myView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    myView.backgroundColor = [UIColor whiteColor];
    myView.frame = [[UIScreen mainScreen] bounds];
    
    customLayer = [[LoadingLayer alloc] init];
    [myView.layer addSublayer:customLayer];
    
    _colorLoadingLayer = [UIColor colorWithWhite:0.6 alpha:1];
    
    self.view = myView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}


- (void)viewWillAppear:(BOOL)animated {
    
    customLayer.backgroundColor = [[UIColor clearColor] CGColor];
    customLayer.frame = CGRectInset(self.view.bounds, 0.0f, 0.0f);
    //int scale3 = self.view.frame.size.width * 0.35;
    int scale3 = self.view.frame.size.width * 0.45;
    customLayer.ovalRect = CGRectMake(scale3 , scale3, self.view.frame.size.width - 2*scale3,  self.view.frame.size.height - 2*scale3);
    //customLayer.lineWidth = self.view.frame.size.width * 0.08;
    customLayer.lineWidth = self.view.frame.size.width * 0.2;
    customLayer.colorLine = _colorLoadingLayer;
    [customLayer setNeedsDisplay];
}

- (void)viewDidAppear:(BOOL)animated {
    
    CABasicAnimation *fullRotation3 = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    fullRotation3.fromValue = [NSNumber numberWithFloat:0];
    fullRotation3.toValue = [NSNumber numberWithFloat:MAXFLOAT];
    fullRotation3.duration = MAXFLOAT * 0.2;
    fullRotation3.removedOnCompletion = YES;
    
    [customLayer addAnimation:fullRotation3 forKey:nil];
    
}

-(void)viewWillDisappear:(BOOL)animated {
    [customLayer removeAllAnimations];
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
	
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
