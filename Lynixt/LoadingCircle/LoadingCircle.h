//
//  MZLoadingCircle.h
//  MZLoading
//
//  Created by Serghei Mazur on 10/31/13.
//  Copyright (c) 2013 Serghei Mazur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


@class LoadingLayer;

@interface LoadingCircle : UIViewController{
    LoadingLayer *customLayer;
}

@property (weak,nonatomic) UIColor *colorLoadingLayer;

@end
