//
//  SettingsViewController.h
//  Lynixt
//
//  Created by Rizwan on 8/6/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "BaseViewController.h"

@interface SettingsViewController : BaseViewController

@property (nonatomic, strong) NSString *strTimeZone;

@end
