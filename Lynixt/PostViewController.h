//
//  PostViewController.h
//  Lynixt
//
//  Created by Rizwan on 9/1/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "DLCPGradientColorPickerController.h"

@interface PostViewController : DLCPGradientColorPickerController

@property (readwrite, assign, nonatomic) BOOL hexVisible;

@end
