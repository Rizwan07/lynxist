//
//  ViewController.m
//  Lynixt
//
//  Created by Rizwan on 7/21/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "ViewController.h"
#import "LoginViewController.h"

@interface ViewController () {
    
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    //[self performSelectorInBackground:@selector(goLogin) withObject:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self performSelector:@selector(goLogin) withObject:nil afterDelay:1.0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)goLogin  {
    
//    dispatch_async(dispatch_get_main_queue(), ^{
    
        LoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [loginVC setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:loginVC animated:YES completion:nil];
//    });
}


@end
