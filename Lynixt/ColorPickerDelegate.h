//
//  ColorPickerDelegate.h
//  Lynixt
//
//  Created by Rizwan on 9/3/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ColorPickerDelegate <NSObject>

- (void)colorPicked:(UIColor )color;

@end
