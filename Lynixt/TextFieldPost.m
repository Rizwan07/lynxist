//
//  TextFieldPost.m
//  Lynixt
//
//  Created by Rizwan on 9/4/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "TextFieldPost.h"

@implementation TextFieldPost

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    
    if ( [UIMenuController sharedMenuController] )
    {
        [UIMenuController sharedMenuController].menuVisible = NO;
        
    }
    return NO;
    
//    if (action == @selector(paste:))
//        return NO;
//    if (action == @selector(select:))
//        return NO;
//    if (action == @selector(selectAll:))
//        return NO;
//    if (action == @selector(cut:)) {
//        return NO;
//    }
//
//    return [super canPerformAction:action withSender:sender];
    
}

- (id)targetForAction:(SEL)action withSender:(id)sender {
    
    if ( [UIMenuController sharedMenuController] )
    {
        [UIMenuController sharedMenuController].menuVisible = NO;
        
    }
    return nil;
    
//    if (action == @selector(paste:))
//        return nil;
//    if (action == @selector(select:))
//        return nil;
//    if (action == @selector(selectAll:))
//        return nil;
//    if (action == @selector(cut:))
//        return nil;
//    
//    return [super targetForAction:action withSender:sender];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
