//
//  ExploreViewController.m
//  Lynixt
//
//  Created by Rizwan on 8/6/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "ExploreViewController.h"

@interface ExploreViewController () {
    
    __weak IBOutlet UIButton *btnBrowseCategory;
    __weak IBOutlet UIButton *btnImportContacts;
}
- (IBAction)topBtnsPressed:(id)sender;

@end

@implementation ExploreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)topBtnsPressed:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    
    [btnBrowseCategory setSelected:btn.tag == 0];
    [btnImportContacts setSelected:btn.tag == 1];
    
    if (btn.tag == 0) {
        
        [btnBrowseCategory setBackgroundColor:[UIColor blackColor]];
        [btnImportContacts setBackgroundColor:[UIColor colorWithRed:193.0/255.0 green:193.0/255.0 blue:193.0/255.0 alpha:1.0]];
    }
    else {
        [btnBrowseCategory setBackgroundColor:[UIColor colorWithRed:193.0/255.0 green:193.0/255.0 blue:193.0/255.0 alpha:1.0]];
        [btnImportContacts setBackgroundColor:[UIColor blackColor]];
    }
    
}
@end
