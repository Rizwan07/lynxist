//
//  SideMenuViewController.m
//  Lynixt
//
//  Created by Rizwan on 7/21/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "SideMenuViewController.h"
#import "ViewController.h"

@interface SideMenuViewController ()

- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)profileBtnPressed:(id)sender;
- (IBAction)exploreBtnPressed:(id)sender;
- (IBAction)followersBtnPressed:(id)sender;
- (IBAction)followingBtnPressed:(id)sender;
- (IBAction)popularAccountsBtnPressed:(id)sender;
- (IBAction)whoToFollowBtnPressed:(id)sender;
- (IBAction)notificationBtnPressed:(id)sender;
- (IBAction)settingsBtnPressed:(id)sender;
- (IBAction)logoutBtnPressed:(id)sender;
@end

@implementation SideMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)homeBtnPressed:(id)sender {
}

- (IBAction)profileBtnPressed:(id)sender {
}

- (IBAction)exploreBtnPressed:(id)sender {
}

- (IBAction)followersBtnPressed:(id)sender {
}

- (IBAction)followingBtnPressed:(id)sender {
}

- (IBAction)popularAccountsBtnPressed:(id)sender {
}

- (IBAction)whoToFollowBtnPressed:(id)sender {
}

- (IBAction)notificationBtnPressed:(id)sender {
}

- (IBAction)settingsBtnPressed:(id)sender {
}

- (IBAction)logoutBtnPressed:(id)sender {
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Are you sure?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Logout", nil];
    [alertView show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 1) {
        
        UIViewController *vc = self.presentingViewController;
        
        while (![vc isKindOfClass:[ViewController class]]) {
            vc = vc.presentingViewController;
        }
        
        [vc dismissViewControllerAnimated:YES completion:nil];
    }
    
}

@end
