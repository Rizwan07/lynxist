//
//  SignupViewController.m
//  Lynixt
//
//  Created by Rizwan on 7/22/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "SignupViewController.h"

#import "SideMenuViewController.h"
#import "SWRevealViewController.h"

@interface SignupViewController ()<UIPickerViewDataSource, UIPickerViewDelegate> {
    
    __weak IBOutlet UIView *viewFullName;
    __weak IBOutlet UIView *viewEmail;
    __weak IBOutlet UIView *viewPassword;
    __weak IBOutlet UIView *viewConfirmPassword;
    __weak IBOutlet UIView *viewUsername;
    __weak IBOutlet UIView *viewPhoneNumber;
    
    
    __weak IBOutlet UITextField *textFieldFullName;
    __weak IBOutlet UITextField *textFieldEmail;
    __weak IBOutlet UITextField *textFieldPassword;
    __weak IBOutlet UITextField *confirmPassword;
    __weak IBOutlet UITextField *textFieldUsername;
    __weak IBOutlet UITextField *textFieldPhoneNumber;
    __weak IBOutlet UITextField *textFieldChooseCategory;
    IBOutlet UITapGestureRecognizer *tapGesture;
    UIPickerView *_pickerView;
    
    NSArray *arrCategory;
    __weak IBOutlet NSLayoutConstraint *scrollHeight;
    __weak IBOutlet UIScrollView *_scrollView;
    __weak IBOutlet UIView *viewActivationCode;
    __weak IBOutlet UITextField *textFieldCode;
    __weak IBOutlet UIButton *btnKeepSignedin;
    
    NSDictionary *dicUser;
    
    Loader *loader;
}

@property(nonatomic,strong) SWRevealViewController *swrevealviewcontroller;

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)keepMeSignedInBtnPressed:(id)sender;
- (IBAction)createAccountBtnPressed:(id)sender;
- (IBAction)tapGestureTapped:(id)sender;

- (IBAction)sendBtnPressed:(id)sender;
- (IBAction)cancelBtnPressed:(id)sender;

- (IBAction)termsOfServicesBtnPressed:(id)sender;
- (IBAction)privacyPolicyBtnPressed:(id)sender;
@end

@implementation SignupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    loader = [[Loader alloc] init];
    [self getCategoryArray];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //[self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)getCategoryArray {
    
    textFieldChooseCategory.text = @"1";
    return;
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
    
    
    NSString *strUrlPath = [NSString stringWithFormat:@"%@/categories", baseURLPath];
    
    [httpClient postPath:strUrlPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dicJson = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
        if ([dicJson[@"status"] isEqualToString:@"TRUE"]) {
            arrCategory = dicJson[@"data"];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:dicJson[@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
}

#pragma mark --
#pragma mark -- UITextField Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [tapGesture setEnabled:NO];
    scrollHeight.constant = 300;
    return [textField resignFirstResponder];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if ([[UIScreen mainScreen] bounds].size.height == 480) {
        scrollHeight.constant = 300 + 110;
    }
    else {
        scrollHeight.constant = 300 + 20;
    }

    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [tapGesture setEnabled:YES];
    
    //scrollHeight.constant = 300 + 100;
    
    if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_6_1) {
        
        // do stuff for iOS 7 and newer
        
    }
    else {
        
        // do stuff for older versions than iOS 7
    }
    
    if ([[UIScreen mainScreen] bounds].size.height == 480) {
        
        if (textField == confirmPassword) {
            
            [_scrollView setContentOffset:CGPointMake(0, 10)];
        }
        else if (textField == textFieldUsername) {

            [_scrollView setContentOffset:CGPointMake(0, 55)];
        }
        else if (textField == textFieldPhoneNumber) {
            
            [_scrollView setContentOffset:CGPointMake(0, 100)];
        }
    }
    else {
        if (textField == textFieldPhoneNumber) {

            [_scrollView setContentOffset:CGPointMake(0, 20)];
        }
    }
    
    if (textField == textFieldChooseCategory) {
        
        if ([arrCategory count] > 0) {
            
            _pickerView = [[UIPickerView alloc] init];//WithFrame:CGRectMake(0, 200, 320, 200)];
            _pickerView.delegate = self;
            _pickerView.dataSource = self;
            _pickerView.showsSelectionIndicator = YES;
            
            textField.inputView = _pickerView;
        }
        else {
            [textField resignFirstResponder];
        }
        if (_scrollView.contentOffset.y == 0) {

            [_scrollView setContentOffset:CGPointMake(0, -140)];
        }
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    
}

- (IBAction)backBtnPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)keepMeSignedInBtnPressed:(id)sender {
    
    btnKeepSignedin.selected = !btnKeepSignedin.selected;
    
}

- (IBAction)createAccountBtnPressed:(id)sender {
    
    if (textFieldFullName.text.length != 0 &&
        textFieldEmail.text.length != 0 &&
        textFieldPassword.text.length != 0 &&
        confirmPassword.text.length != 0 &&
        textFieldUsername.text.length != 0) {
        //&& textFieldPhoneNumber.text.length != 0) {
        
        if (![self emailFormat:textFieldEmail.text]) {
            [self shakeView:viewEmail];
        }
        else {
            if ([textFieldPassword.text isEqualToString:confirmPassword.text]) {
                
                [loader startAnimatingWithViewController:self];
                AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
                
                NSDictionary *dic = @{@"full_name": textFieldFullName.text,
                                      @"email": textFieldEmail.text,
                                      @"password": textFieldPassword.text,
                                      @"username": textFieldUsername.text,
                                      //@"mobile": textFieldPhoneNumber.text,
                                      @"platform": @"iphone",
                                      @"category_id": textFieldChooseCategory.text,
                                      @"latitute": @"212121",
                                      @"longitude": @"56565656"};
                
                NSString *strUrlPath = [NSString stringWithFormat:@"%@/signUp", baseURLPath];
                
                [httpClient getPath:strUrlPath parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    [loader stopAnimating];
                    
                    NSDictionary *dicJson = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
                    dicUser = dicJson[@"finalData"];
                    NSLog(@"data %@", dicUser);
                    if ([dicJson[@"response"] isEqualToString:@"SUCCESS"]) {
                        
                        [self enterTotheApp];
//                        if ([dicJson[@"finalData"][@"verify_status"] boolValue]) {
//                            
//                            [self enterTotheApp];
//                        }
//                        else {
//                            [UIView transitionWithView:viewActivationCode duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
//                                [viewActivationCode setAlpha:1.0];
//                            } completion:^(BOOL finished) {
//                                
//                            }];
//                        }
                        
                    }
                    else {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:dicJson[@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                    }
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    [loader stopAnimating];
                }];
            }
            else {
                [self shakeView:viewPassword];
                [self shakeView:viewConfirmPassword];
            }
        }
    }
    else {
        if (textFieldFullName.text.length == 0) {
            [self shakeView:viewFullName];
        }
        if (textFieldEmail.text.length == 0) {
            [self shakeView:viewEmail];
        }
        if (textFieldPassword.text.length == 0) {
            [self shakeView:viewPassword];
        }
        if (confirmPassword.text.length == 0) {
            [self shakeView:viewConfirmPassword];
        }
        if (textFieldUsername.text.length == 0) {
            [self shakeView:viewUsername];
        }
        if (textFieldPhoneNumber.text.length == 0) {
            [self shakeView:viewPhoneNumber];
        }
    }
}

- (void)enterTotheApp {
    
//    SideMenuViewController *leftMenuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SideMenuViewController"];
//    //HomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
//    UINavigationController *templateMenuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
//    
//    self.swrevealviewcontroller = [[SWRevealViewController alloc] initWithRearViewController:leftMenuVC frontViewController:templateMenuVC];
//    [self.swrevealviewcontroller setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
//    [self presentViewController:self.swrevealviewcontroller animated:YES completion:nil];
    
    SideMenuViewController *leftMenuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SideMenuViewController"];
    UITabBarController *templateMenuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarController"];
    
    UITabBar *tabBar = templateMenuVC.tabBar;
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
    UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
    UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
    UITabBarItem *tabBarItem5 = [tabBar.items objectAtIndex:4];
    
    [tabBarItem1 setFinishedSelectedImage:[UIImage imageNamed:@"tab1-home-selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab1-home-normal.png"]];
    [tabBarItem2 setFinishedSelectedImage:[UIImage imageNamed:@"tab2-explore-selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab2-explore-normal.png"]];
    [tabBarItem3 setFinishedSelectedImage:[UIImage imageNamed:@"tab3-more-selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab3-more-normal.png"]];
    [tabBarItem4 setFinishedSelectedImage:[UIImage imageNamed:@"tab4-notification-selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab4-notification-normal.png"]];
    [tabBarItem5 setFinishedSelectedImage:[UIImage imageNamed:@"tab5-profile-selected.png"] withFinishedUnselectedImage:[UIImage imageNamed:@"tab5-profile-normal.png"]];
    
    
    self.swrevealviewcontroller = [[SWRevealViewController alloc] initWithRearViewController:leftMenuVC frontViewController:templateMenuVC];
    [self.swrevealviewcontroller setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:self.swrevealviewcontroller animated:YES completion:nil];
    
    CGRect rect = templateMenuVC.tabBar.frame;
    //rect.origin.y += 16;
    CGRect newRect = CGRectMake(rect.origin.x, rect.origin.y + 10, rect.size.width, rect.size.height); //fix UITabBarItem bug
    templateMenuVC.tabBar.frame = newRect;
    
    if (!(floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)) {
        // here you go with iOS 7
        if ([[UIScreen mainScreen] bounds].size.height == 480) {
            
            [self hideTabBar:templateMenuVC withMargin:0];
        }
        else {
            [self hideTabBar:templateMenuVC withMargin:88];
            
        }
    }

}


- (IBAction)tapGestureTapped:(id)sender {
    
    scrollHeight.constant = 300;
    
    [textFieldFullName resignFirstResponder];
    [textFieldEmail resignFirstResponder];
    [textFieldPassword resignFirstResponder];
    [confirmPassword resignFirstResponder];
    [textFieldUsername resignFirstResponder];
    [textFieldChooseCategory resignFirstResponder];
    [textFieldPhoneNumber resignFirstResponder];
    [tapGesture setEnabled:NO];
}

- (IBAction)sendBtnPressed:(id)sender {
    
    if (textFieldCode.text.length == 0) {
        
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
        [animation setDuration:.08];
        [animation setRepeatCount:5];
        [animation setAutoreverses:YES];
        [animation setFromValue:[NSValue valueWithCGPoint:
                                 CGPointMake([textFieldCode center].x - 5.0f, [textFieldCode center].y)]];
        [animation setToValue:[NSValue valueWithCGPoint:
                               CGPointMake([textFieldCode center].x + 5.0f, [textFieldCode center].y)]];
        
        [[textFieldCode layer] addAnimation:animation forKey:@"position"];
        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Field required" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
    }
    else {
        if ([dicUser[@"verify_code"] isEqualToString:textFieldCode.text]) {
            
            AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
            
            NSDictionary *dic = @{@"user_id": dicUser[@"user_id"],
                                  @"verify_code": textFieldCode.text};
            
            NSString *strUrlPath = [NSString stringWithFormat:@"%@/activateUserAccount", baseURLPath];
            
            [httpClient postPath:strUrlPath parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
                
                [textFieldCode resignFirstResponder];
                NSDictionary *dicJson = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
                
                if ([dicJson[@"response"] isEqualToString:@"SUCCESS"]) {
                    
                    [UIView transitionWithView:viewActivationCode duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                        [viewActivationCode setAlpha:0.0];
                    } completion:^(BOOL finished) {
                        
                    }];
                    
//                    NSDictionary *dic = @{@"user_email": dicUser[@"email"],
//                                          @"user_name": dicUser[@"user_name"],
//                                          @"full_name": dicUser[@"full_name"],
//                                          @"user_id": dicUser[@"user_id"]};
                    
                    [[NSUserDefaults standardUserDefaults] setObject:dicUser forKey:@"user_data"];
                    [[NSUserDefaults standardUserDefaults] synchronize];

                }
                else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:dicJson[@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [textFieldCode resignFirstResponder];
            }];
            
            [self enterTotheApp];
        }
        else {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Not a valid code" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

- (IBAction)cancelBtnPressed:(id)sender {
    
    [textFieldCode resignFirstResponder];
    [UIView transitionWithView:viewActivationCode duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [viewActivationCode setAlpha:0.0];
    } completion:^(BOOL finished) {
        
    }];

}

- (IBAction)termsOfServicesBtnPressed:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://developer.avenuesocial.com/azeemsal/lynixst/term_condition.html"]];
}

- (IBAction)privacyPolicyBtnPressed:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://developer.avenuesocial.com/azeemsal/lynixst/privacy_policy.html"]];
}

- (void)hideTabBar:(UITabBarController *) tabbarcontroller withMargin:(NSInteger )margin
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    
    for(UIView *view in tabbarcontroller.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, 421+margin, view.frame.size.width, view.frame.size.height)];
        }
        else
        {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 421+margin)];
        }
    }
    
    [UIView commitAnimations];
}


#pragma mark --
#pragma mark -- UIPickerView Delegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    [textFieldChooseCategory setText:arrCategory[row][@"category_name"]];
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSUInteger numRows = 5;
    
    return numRows;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *title;
    title = arrCategory[row][@"category_name"];
    
    return title;
}

@end
