//
//  SettingsViewController.m
//  Lynixt
//
//  Created by Rizwan on 8/6/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController () <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    
    __weak IBOutlet UILabel *lblHeaderSettings;
    __weak IBOutlet UIPageControl *_pageControl;
    __weak IBOutlet UIScrollView *scrollViewMain;
    __weak IBOutlet UIScrollView *scrollViewAccount;
    __weak IBOutlet UIScrollView *scrollViewProfile;
    __weak IBOutlet NSLayoutConstraint *scrollContentHeightAccount;
    __weak IBOutlet NSLayoutConstraint *scrollContentHeightProfile;
    
    __weak IBOutlet NSLayoutConstraint *scroll1Height;
    __weak IBOutlet NSLayoutConstraint *scroll2Height;
    __weak IBOutlet NSLayoutConstraint *scroll3Height;
    __weak IBOutlet NSLayoutConstraint *scroll4Height;
    __weak IBOutlet NSLayoutConstraint *scrollHeightForiPhone4;
    IBOutlet UITapGestureRecognizer *tapGesture;
    NSArray *arrHeadings;
    
    // Account
    __weak IBOutlet UITextField *textFieldName;
    __weak IBOutlet UITextField *textFieldEmail;
    __weak IBOutlet UITextField *textFieldCountry;
    __weak IBOutlet UITextField *textFieldTimeZone;
    
    __weak IBOutlet UITextField *textFieldCurrentPassword;
    __weak IBOutlet UITextField *textFieldNewPassword;
    __weak IBOutlet UITextField *textFieldVerifyPassword;
    
    // Email Notification
    __weak IBOutlet UIButton *btnN1PostMarkApproved;
    __weak IBOutlet UIButton *btnN2PostMentionMarkApproved;
    __weak IBOutlet UIButton *btnN3PostsRepost;
    __weak IBOutlet UIButton *btnN4PostMentionRepost;
    __weak IBOutlet UIButton *btnN5PostGetReplyOrMentionInPost;
    __weak IBOutlet UIButton *btnN6FollowedBySomeOneNew;
    __weak IBOutlet UIButton *btnN7SomeOneSharePostWithMe;
    __weak IBOutlet UIButton *btnN8SomeOneFromAddressBookJoinLynxist;
    __weak IBOutlet UIButton *btnN9SentDirectMessage;
    __weak IBOutlet UIButton *btnN10PostMarkApproved;
    __weak IBOutlet UIButton *btnN11PostsRepost;
    __weak IBOutlet UIButton *btnN12NewsProductFeatureUpdate;
    __weak IBOutlet UIButton *btnN13TipsOnGettingMorePost;
    __weak IBOutlet UIButton *btnN14ThingsMissedSinceLastLoggedin;
    __weak IBOutlet UIButton *btnN15ParticipationResearchServey;
    __weak IBOutlet UIButton *btnN16SuggestionPplUMayKnowOnLynxist;
    __weak IBOutlet UIButton *btnN17SuggestionBasedMyRecentFollow;
    
    // Security & Privacy
    __weak IBOutlet UIButton *btnS1DontVerifyLoginRequest;
    __weak IBOutlet UIButton *btnS2SendLoginVerificationReqMyPhone;
    __weak IBOutlet UIButton *btnS3ReqPersonalInfoResetPassword;
    __weak IBOutlet UIButton *btnS4ProtectPosts;
    __weak IBOutlet UIButton *btnS5AddLocationToMyPost;
    __weak IBOutlet UIButton *btnS6FindMeByEmail;
    __weak IBOutlet UIButton *btnS7TailorRecentWebsiteVisit;
    __weak IBOutlet UIButton *btnS8TailorAdsShareByAdPartner;
    
    
    // Profile
    __weak IBOutlet UIImageView *imageViewProfile;
    __weak IBOutlet UILabel *lblFullName;
    __weak IBOutlet UILabel *lblUsername;
    __weak IBOutlet UILabel *lblAbout;
    __weak IBOutlet UITextField *textFieldProfileName;
    __weak IBOutlet UITextField *textFieldProfileLocation;
    __weak IBOutlet UITextField *textFieldProfileWebsite;
    __weak IBOutlet UILabel *lblKeywords;
    __weak IBOutlet UITextView *textViewProfileKeyword;
    __weak IBOutlet UITextField *textFieldProfileRssFeed;
    
    UIImagePickerController *imagePickerController;
    
    UITextField *textFieldTemp;
}
- (IBAction)backBtnPressed:(id)sender;

- (IBAction)pageControlTapped:(id)sender;

- (IBAction)emailNotificationBtnsPressed:(id)sender;
- (IBAction)securityAndPrivacyBtnsPressed:(id)sender;

- (IBAction)tapGestureTapped:(UITapGestureRecognizer *)sender;
- (IBAction)accountCancelBtnPressed:(id)sender;
- (IBAction)accountSaveBtnPressed:(id)sender;
- (IBAction)notificationSaveBtnPressed:(id)sender;
- (IBAction)securitySaveBtnPressed:(id)sender;
- (IBAction)profileBtnPressed:(id)sender;
- (IBAction)profileSaveBtnPressed:(id)sender;
- (IBAction)profileCancelBtnPressed:(id)sender;

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    arrHeadings = @[@"Account", @"Email Notifications", @"Security & Privacy", @"Profile"];
    
    if ([[UIScreen mainScreen] bounds].size.height == 480) {
        
        scrollHeightForiPhone4.constant = 418 - 88;
    }
    
    _strTimeZone = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"user_data.time_zone"];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if ([[UIScreen mainScreen] bounds].size.height == 480) {
        
        scrollHeightForiPhone4.constant = 418 - 88;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [self getNotificationAndSecurityChecks];
    
    imagePickerController = [[UIImagePickerController alloc] init];
    [imagePickerController setDelegate:self];
    [imagePickerController setAllowsEditing:YES];

    //Account
    
    [textFieldName setText:[defaults valueForKeyPath:@"user_data.full_name"]];
    [textFieldEmail setText:[defaults valueForKeyPath:@"user_data.email"]];
    [textFieldCountry setText:[defaults valueForKeyPath:@"user_data.country_code"]];
    [textFieldTimeZone setText:_strTimeZone];
    
    // Profile
    
    [lblFullName setText:[defaults valueForKeyPath:@"user_data.full_name"]];
    [lblUsername setText:[defaults valueForKeyPath:@"user_data.user_name"]];
    [lblAbout setText:[defaults valueForKeyPath:@"user_data.description"]];

    [textFieldProfileName setText:[defaults valueForKeyPath:@"user_data.full_name"]];
    [textFieldProfileLocation setText:[defaults valueForKeyPath:@"user_data.location"]];
    [textFieldProfileWebsite setText:[defaults valueForKeyPath:@"user_data.web"]];
    [textViewProfileKeyword setText:[defaults valueForKeyPath:@"user_data.description"]];
    [lblKeywords setHidden:[[defaults valueForKeyPath:@"user_data.description"] length]];
    [textFieldProfileRssFeed setText:[defaults valueForKeyPath:@"user_data.rss"]];

    if ([defaults valueForKeyPath:@"user_data.profile_pic"] != nil) {
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[defaults valueForKeyPath:@"user_data.profile_pic"]]];
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            if (data && !connectionError) {
                [imageViewProfile setImage:[UIImage imageWithData:data]];
            }
        }];
    }
    
}

- (void)getNotificationAndSecurityChecks {
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
    
    NSString *strUrlPath = [NSString stringWithFormat:@"%@/securityPrivacy", baseURLPath];
    
    NSString *strUserId = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"user_data.user_id"];
    
    NSDictionary *dic = @{@"user_id": strUserId,
                          @"update_process": @"off"};
    
    [httpClient postPath:strUrlPath parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [textFieldTemp resignFirstResponder];
        NSDictionary *dicJson = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
        if ([dicJson[@"response"] isEqualToString:@"SUCCESS"]) {
            NSDictionary *dic = dicJson[@"finalData"];
            [btnN1PostMarkApproved setSelected:[dic[@"posts_marked_as_approve"] boolValue]];
            [btnN2PostMentionMarkApproved setSelected:[dic[@"post_mentioned_marked_as_approve"] boolValue]];
            [btnN3PostsRepost setSelected:[dic[@"posts_repost"] boolValue]];
            [btnN4PostMentionRepost setSelected:[dic[@"posts_mentioned_repost"] boolValue]];
            [btnN5PostGetReplyOrMentionInPost setSelected:[dic[@"post_get_reply_or_mentioned_post"] boolValue]];
            [btnN6FollowedBySomeOneNew setSelected:[dic[@"followed_someone_new"] boolValue]];
            [btnN7SomeOneSharePostWithMe setSelected:[dic[@"someone_shares_post_me"] boolValue]];
            [btnN8SomeOneFromAddressBookJoinLynxist setSelected:[dic[@"someone_add_book_joins_lynxist"] boolValue]];
            [btnN9SentDirectMessage setSelected:[dic[@"sent_direct_message"] boolValue]];
            [btnN10PostMarkApproved setSelected:[dic[@"repost_marked_approve"] boolValue]];
            [btnN11PostsRepost setSelected:[dic[@"reposts_reposted"] boolValue]];
            [btnN12NewsProductFeatureUpdate setSelected:[dic[@"news_product_and_feature_updates"] boolValue]];
            [btnN13TipsOnGettingMorePost setSelected:[dic[@"tips_on_getting_more_out_post"] boolValue]];
            [btnN14ThingsMissedSinceLastLoggedin setSelected:[dic[@"things_missed_since_last_logged"] boolValue]];
            [btnN15ParticipationResearchServey setSelected:[dic[@"participation_research_surveys"] boolValue]];
            [btnN16SuggestionPplUMayKnowOnLynxist setSelected:[dic[@"suggestions_about_people_may_know_on_lynxist"] boolValue]];
            [btnN17SuggestionBasedMyRecentFollow setSelected:[dic[@"suggestions_based_recent_follows"] boolValue]];
            
            [btnS1DontVerifyLoginRequest setSelected:[dic[@"dt_verify_login_req"] boolValue]];
            [btnS2SendLoginVerificationReqMyPhone setSelected:[dic[@"send_login_verif_req_phone"] boolValue]];
            [btnS3ReqPersonalInfoResetPassword setSelected:[dic[@"personal_info_reset_pass"] boolValue]];
            [btnS4ProtectPosts setSelected:[dic[@"protect_posts"] boolValue]];
            [btnS5AddLocationToMyPost setSelected:[dic[@"add_location_to_posts"] boolValue]];
            [btnS6FindMeByEmail setSelected:[dic[@"find_by_email"] boolValue]];
            [btnS7TailorRecentWebsiteVisit setSelected:[dic[@"tailor_recent_web_visits"] boolValue]];
            [btnS8TailorAdsShareByAdPartner setSelected:[dic[@"tailor_ads_infor_shared_ad_partners"] boolValue]];
        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:dicJson[@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //        [loader stopAnimating];
        [textFieldTemp resignFirstResponder];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if (page >= 0 && page < 4) {
        
        _pageControl.currentPage = page;
        [lblHeaderSettings setText:arrHeadings[page]];
    }
    [self tapGestureTapped:tapGesture];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [tapGesture setEnabled:NO];
    scrollContentHeightAccount.constant = 568;
    scrollContentHeightProfile.constant = 580;
    
    return [textField resignFirstResponder];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if ([[UIScreen mainScreen] bounds].size.height == 480) {
        scrollContentHeightAccount.constant = 568 + 140;
        scrollContentHeightProfile.constant = 580 + 100;
    }
    else {
        scrollContentHeightAccount.constant = 568 + 110;
        scrollContentHeightProfile.constant = 580 + 50;
    }
    
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [tapGesture setEnabled:YES];
    textFieldTemp = textField;
    
    if ([[UIScreen mainScreen] bounds].size.height == 480) {
        
        if (textField == textFieldProfileName) {
            
            [scrollViewProfile setContentOffset:CGPointMake(0, 20)];
        }
        if (textField == textFieldProfileLocation) {
            
            [scrollViewProfile setContentOffset:CGPointMake(0, 90)];
        }
        if (textField == textFieldProfileWebsite) {
            
            [scrollViewProfile setContentOffset:CGPointMake(0, 160)];
        }
        if (textField == textFieldProfileRssFeed) {
            
            [scrollViewProfile setContentOffset:CGPointMake(0, 300)];
        }

        
        
        if (textField == textFieldEmail) {
            
            [scrollViewAccount setContentOffset:CGPointMake(0, 10)];
        }
        if (textField == textFieldCountry) {
            
            [scrollViewAccount setContentOffset:CGPointMake(0, 70)];
        }
        if (textField == textFieldTimeZone) {
            
            [scrollViewAccount setContentOffset:CGPointMake(0, 130)];
        }
        if (textField == textFieldCurrentPassword) {
            
            [scrollViewAccount setContentOffset:CGPointMake(0, 240)];
        }
        if (textField == textFieldNewPassword) {
            
            [scrollViewAccount setContentOffset:CGPointMake(0, 310)];
        }
        if (textField == textFieldVerifyPassword) {
            
            [scrollViewAccount setContentOffset:CGPointMake(0, 380)];
        }

        
    }
    else {
        if (textField == textFieldProfileWebsite) {
            
            [scrollViewProfile setContentOffset:CGPointMake(0, 40)];
        }
        if (textField == textFieldProfileRssFeed) {
            
            [scrollViewProfile setContentOffset:CGPointMake(0, 210)];
        }
        
        if (textField == textFieldTimeZone) {
            
            [scrollViewAccount setContentOffset:CGPointMake(0, 40)];
        }
        if (textField == textFieldCurrentPassword) {
            
            [scrollViewAccount setContentOffset:CGPointMake(0, 150)];
        }
        if (textField == textFieldNewPassword) {
            
            [scrollViewAccount setContentOffset:CGPointMake(0, 210)];
        }
        if (textField == textFieldVerifyPassword) {
            
            [scrollViewAccount setContentOffset:CGPointMake(0, 260)];
        }
        
    }

}

- (IBAction)backBtnPressed:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)pageControlTapped:(id)sender {
    
    [scrollViewMain scrollRectToVisible:CGRectMake(_pageControl.currentPage*320, 0, 320, 200) animated:YES];
}

- (IBAction)emailNotificationBtnsPressed:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    [button setSelected:![button isSelected]];
}

- (IBAction)securityAndPrivacyBtnsPressed:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    [button setSelected:![button isSelected]];
}

- (IBAction)tapGestureTapped:(UITapGestureRecognizer *)sender {
    
    scrollContentHeightAccount.constant = 568;
    scrollContentHeightProfile.constant = 580;
    
    [textFieldTemp resignFirstResponder];
    [textViewProfileKeyword resignFirstResponder];
    
    [tapGesture setEnabled:NO];
}

- (IBAction)accountCancelBtnPressed:(id)sender {
    
    textFieldCurrentPassword.text = textFieldNewPassword.text = textFieldVerifyPassword.text = @"";
}

- (IBAction)accountSaveBtnPressed:(id)sender {
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
    
    NSString *strUrlPath = [NSString stringWithFormat:@"%@/editAccount", baseURLPath];
    
    NSString *strUserId = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"user_data.user_id"];
    
    NSDictionary *dic;
    
    if (textFieldCurrentPassword.text.length == 0 && textFieldNewPassword.text.length == 0 && textFieldVerifyPassword.text.length == 0) {
        
        dic = @{@"user_id": strUserId,
                              @"user_name": textFieldName.text,
                              @"email": textFieldEmail.text,
                              @"country_code": textFieldCountry.text,
                              @"timezone": textFieldTimeZone.text};
    }
    else {
        if (textFieldCurrentPassword.text.length != 0 && textFieldNewPassword.text.length != 0 && textFieldVerifyPassword.text.length != 0) {
            dic = @{@"user_id": strUserId,
                                  @"user_name": textFieldName.text,
                                  @"email": textFieldEmail.text,
                                  @"country_code": textFieldCountry.text,
                                  @"timezone": textFieldTimeZone.text,
                                  @"c_password": textFieldCurrentPassword.text,
                                  @"n_password": textFieldNewPassword.text,
                                  @"v_password": textFieldVerifyPassword.text};
        }
        else {
            if (textFieldCurrentPassword.text.length == 0) {
                [self shakeView:textFieldCurrentPassword];
            }
            if (textFieldNewPassword.text.length == 0) {
                [self shakeView:textFieldNewPassword];
            }
            if (textFieldVerifyPassword.text.length == 0) {
                [self shakeView:textFieldVerifyPassword];
            }
            if (textFieldNewPassword.text.length != 0 && textFieldVerifyPassword.text.length != 0 && ![textFieldNewPassword.text isEqualToString:textFieldVerifyPassword.text]) {
                [self shakeView:textFieldNewPassword];
                [self shakeView:textFieldVerifyPassword];
            }
            return;
        }
        
    }
    
    [httpClient postPath:strUrlPath parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [textFieldTemp resignFirstResponder];
        NSDictionary *dicJson = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
        if ([dicJson[@"response"] isEqualToString:@"SUCCESS"]) {
            
            [[NSUserDefaults standardUserDefaults] setObject:dicJson[@"finalData"] forKey:@"user_data"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:dicJson[@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];

        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:dicJson[@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //        [loader stopAnimating];
        [textFieldTemp resignFirstResponder];
    }];
}

- (IBAction)notificationSaveBtnPressed:(id)sender {
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
    
    NSString *strUrlPath = [NSString stringWithFormat:@"%@/emailNotification", baseURLPath];
    
    NSString *strUserId = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"user_data.user_id"];
    
    NSDictionary *dic = @{@"user_id": strUserId,
                          @"posts_marked_as_approve":                       @([btnN1PostMarkApproved isSelected]),
                          @"post_mentioned_marked_as_approve":              @([btnN2PostMentionMarkApproved isSelected]),
                          @"posts_repost":                                  @([btnN3PostsRepost isSelected]),
                          @"posts_mentioned_repost":                        @([btnN4PostMentionRepost isSelected]),
                          @"post_get_reply_or_mentioned_post":              @([btnN5PostGetReplyOrMentionInPost isSelected]),
                          @"followed_someone_new":                          @([btnN6FollowedBySomeOneNew isSelected]),
                          @"someone_shares_post_me":                        @([btnN7SomeOneSharePostWithMe isSelected]),
                          @"someone_add_book_joins_lynxist":                @([btnN8SomeOneFromAddressBookJoinLynxist isSelected]),
                          @"sent_direct_message":                           @([btnN9SentDirectMessage isSelected]),
                          @"repost_marked_approve":                         @([btnN10PostMarkApproved isSelected]),
                          @"reposts_reposted":                              @([btnN11PostsRepost isSelected]),
                          //@"post_get_reply_or_mentioned_post":            @([btnN11PostsRepost isSelected]),
                          @"news_product_and_feature_updates":              @([btnN12NewsProductFeatureUpdate isSelected]),
                          @"tips_on_getting_more_out_post":                 @([btnN13TipsOnGettingMorePost isSelected]),
                          @"things_missed_since_last_logged":               @([btnN14ThingsMissedSinceLastLoggedin isSelected]),
                          @"participation_research_surveys":                @([btnN15ParticipationResearchServey isSelected]),
                          @"suggestions_about_people_may_know_on_lynxist":  @([btnN16SuggestionPplUMayKnowOnLynxist isSelected]),
                          @"suggestions_based_recent_follows":              @([btnN17SuggestionBasedMyRecentFollow isSelected])};
    
    [httpClient postPath:strUrlPath parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [textFieldTemp resignFirstResponder];
        NSDictionary *dicJson = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
//        if ([dicJson[@"response"] isEqualToString:@"SUCCESS"]) {
//            
//            
//        }
//        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:dicJson[@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
//        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //        [loader stopAnimating];
        [textFieldTemp resignFirstResponder];
    }];
}

- (IBAction)securitySaveBtnPressed:(id)sender {
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
    
    NSString *strUrlPath = [NSString stringWithFormat:@"%@/securityPrivacy", baseURLPath];
    
    NSString *strUserId = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"user_data.user_id"];
    
    NSDictionary *dic = @{@"user_id": strUserId,
                          @"dt_verify_login_req":                   @([btnS1DontVerifyLoginRequest isSelected]),
                          @"send_login_verif_req_phone":            @([btnS2SendLoginVerificationReqMyPhone isSelected]),
                          //@"send_login_verif_req_lynxist_app":    @([textFieldCountry.text isSelected]),
                          @"personal_info_reset_pass":              @([btnS3ReqPersonalInfoResetPassword isSelected]),
                          @"protect_posts":                         @([btnS4ProtectPosts isSelected]),
                          @"add_location_to_posts":                 @([btnS5AddLocationToMyPost isSelected]),
                          @"find_by_email":                         @([btnS6FindMeByEmail isSelected]),
                          @"tailor_recent_web_visits":              @([btnS7TailorRecentWebsiteVisit isSelected]),
                          @"tailor_ads_infor_shared_ad_partners":   @([btnS8TailorAdsShareByAdPartner isSelected])};
    
    [httpClient postPath:strUrlPath parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [textFieldTemp resignFirstResponder];
        NSDictionary *dicJson = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
//        if ([dicJson[@"response"] isEqualToString:@"SUCCESS"]) {
//            
//            
//        }
//        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:dicJson[@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
//        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //        [loader stopAnimating];
        [textFieldTemp resignFirstResponder];
    }];

}

- (IBAction)profileBtnPressed:(id)sender {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo", @"Photo Gallery", nil];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
            [self presentViewController:imagePickerController animated:YES completion:nil];
        }
    }
    if (buttonIndex == 1) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            [imagePickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
            [self presentViewController:imagePickerController animated:YES completion:nil];
        }
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    imageViewProfile.image = image;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)profileSaveBtnPressed:(id)sender {
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
    
    NSString *strUrlPath = [NSString stringWithFormat:@"%@/editProfile", baseURLPath];
    
    NSString *strUserId = [[NSUserDefaults standardUserDefaults] valueForKeyPath:@"user_data.user_id"];
    
    NSDictionary *dic = @{@"user_id": strUserId,
                          @"user_name": textFieldProfileName.text,
                          @"location": textFieldProfileLocation.text,
                          @"website": textFieldProfileWebsite.text,
                          @"rss": textFieldProfileRssFeed.text,
                          @"desc": textViewProfileKeyword.text,
                          @"images": [UIImageJPEGRepresentation(imageViewProfile.image, 50) base64Encoding]};
    
    [httpClient postPath:strUrlPath parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [textFieldTemp resignFirstResponder];
        NSDictionary *dicJson = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:nil];
        if ([dicJson[@"response"] isEqualToString:@"SUCCESS"]) {
            
            NSLog(@"data %@", dicJson);
            
            [[NSUserDefaults standardUserDefaults] setObject:dicJson[@"finalData"] forKey:@"user_data"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:dicJson[@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];

        }
        else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:dicJson[@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        [loader stopAnimating];
        [textFieldTemp resignFirstResponder];
    }];

}

- (IBAction)profileCancelBtnPressed:(id)sender {
}

#pragma mark --
#pragma mark -- UITextView Delegates

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    
    if ([[UIScreen mainScreen] bounds].size.height == 480) {
        scrollContentHeightAccount.constant = 568 + 140;
        scrollContentHeightProfile.constant = 580 + 100;
    }
    else {
        scrollContentHeightAccount.constant = 568 + 110;
        scrollContentHeightProfile.constant = 580 + 50;
    }
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    [tapGesture setEnabled:YES];
    
    if ([[UIScreen mainScreen] bounds].size.height == 480) {
        
        [scrollViewProfile setContentOffset:CGPointMake(0, 240)];
    }
    else {
        [scrollViewProfile setContentOffset:CGPointMake(0, 160)];
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    [lblKeywords setHidden:textView.text.length!=0];
}

//- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
//    
//    if ([text isEqualToString:@""]) {
//        if ([textView.text length] > 0) {
//            
//            [lblText setText:[[textView text] substringToIndex:[[textView text] length] -1]];
//            if (textView.text.length == 1) {
//                //[lblText setText:mArr[rowAddedAtIndex-1][@"obj"]];
//                [lblText setText:[self getObjectForTag:[_dicTemplate[@"arrayList"][rowAddedAtIndex-1][@"tag"] intValue]]];
//            }
//        }
//    }
//    else {
//        [lblText setText:[NSString stringWithFormat:@"%@%@", [textView text], text]];
//    }
//    
//    return YES;
//}
//
//- (void)textViewDidEndEditing:(UITextView *)textView {
//    if ([textView.text length] != 0) {
//        
//        [_dicTemplate[@"arrayList"][rowAddedAtIndex-1] setObject:textView.text forKey:@"name"];
//    }
//}


@end
