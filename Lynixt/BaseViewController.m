//
//  BaseViewController.m
//  Lynixt
//
//  Created by Rizwan on 7/21/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController () {
    
    __weak IBOutlet UIButton *btnMenu;
}

@end

@implementation BaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    self.view.frame = CGRectMake(0, 0, 320, self.view.frame.size.height+10);
    
    [super viewDidLoad];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (btnMenu != nil) {
        
        [btnMenu addTarget:self.revealViewController action:@selector(revealToggleAnimated:) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationController.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

#pragma mark - Email Validation

-(BOOL)emailFormat:(NSString *)emailText
{
    
    BOOL status;
    status = YES;
    NSString *emailString;
    //emailString = emailAddress.text;
    emailString = emailText;
    NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailReg];
	
    if (([emailTest evaluateWithObject:emailString] != YES) || [emailString isEqualToString:@""])
    {
        status =  NO;
    }
	
    return status;
}

- (void)shakeView:(UIView *)view {
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:.08];
    [animation setRepeatCount:5];
    [animation setAutoreverses:YES];
    [animation setFromValue:[NSValue valueWithCGPoint:
                             CGPointMake([view center].x - 5.0f, [view center].y)]];
    [animation setToValue:[NSValue valueWithCGPoint:
                           CGPointMake([view center].x + 5.0f, [view center].y)]];
    
    [[view layer] addAnimation:animation forKey:@"position"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
