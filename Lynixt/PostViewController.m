//
//  PostViewController.m
//  Lynixt
//
//  Created by Rizwan on 9/1/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import "PostViewController.h"

@interface PostViewController () <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    
    __weak IBOutlet UITextField *textfieldComment;
    __weak IBOutlet UIView *viewOptions;
    __weak IBOutlet UILabel *lblWriteHere;
    __weak IBOutlet UITextView *_textView;
    __weak IBOutlet UIView *viewmeeee;
    __weak IBOutlet UIView *viewColor;
    __weak IBOutlet UIView *viewBg;
    __weak IBOutlet UIImageView *_imageView;
    __weak IBOutlet UIButton *btnMenu;
    UIImagePickerController *imagePickerController;
}

- (IBAction)cameraBtnPressed:(id)sender;
- (IBAction)chainBtnPressed:(id)sender;
- (IBAction)paintBtnPressed:(id)sender;
- (IBAction)colorDoneBtnPressed:(id)sender;

@end

@implementation PostViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [btnMenu addTarget:self.revealViewController action:@selector(revealToggleAnimated:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    //[view setBackgroundColor:[UIColor greenColor]];
    
    UIButton *btnCamera = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnCamera setFrame:CGRectMake(0, 0, 50, 50)];
    [btnCamera setImage:[UIImage imageNamed:@"camera"] forState:UIControlStateNormal];
    [btnCamera addTarget:self action:@selector(cameraBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btnCamera];
    
    UIButton *btnChain = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnChain setFrame:CGRectMake(160-25, 0, 50, 50)];
    [btnChain setImage:[UIImage imageNamed:@"chain"] forState:UIControlStateNormal];
    [btnChain addTarget:self action:@selector(chainBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btnChain];
    
    UIButton *btnPaint = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnPaint setFrame:CGRectMake(320-50, 0, 50, 50)];
    [btnPaint setImage:[UIImage imageNamed:@"paint"] forState:UIControlStateNormal];
    [btnPaint addTarget:self action:@selector(paintBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btnPaint];
    
    [_textView setInputAccessoryView:view];
    
    [[UITextField appearanceWhenContainedIn:[self class], nil] setTintColor:[UIColor clearColor]];
    
    imagePickerController = [[UIImagePickerController alloc] init];
    [imagePickerController setDelegate:self];
    [imagePickerController setAllowsEditing:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(colorChanged:) name:@"ColorChanged" object:nil];

}

- (IBAction)cameraBtnPressed:(id)sender {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo", @"Photo Gallery", nil];
    [actionSheet showFromTabBar:self.tabBarController.tabBar];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
            [self presentViewController:imagePickerController animated:YES completion:nil];
        }
    }
    if (buttonIndex == 1) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            [imagePickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
            [self presentViewController:imagePickerController animated:YES completion:nil];
        }
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    NSLog(@"image %@", image);
    [_imageView setImage:image];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)chainBtnPressed:(id)sender {
    
    NSLog(@"chain");
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter the url" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [alertView setAlertViewStyle:UIAlertViewStylePlainTextInput];
    [[alertView textFieldAtIndex:0] setPlaceholder:@"link"];
    [alertView show];
}

- (IBAction)paintBtnPressed:(id)sender {
    
    [viewColor setHidden:NO];
    NSLog(@"paint");
}

- (IBAction)colorDoneBtnPressed:(id)sender {
    
    [viewColor setHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)colorChanged:(NSNotificationCenter *)notificationCenter {
    
    UIColor *color = (UIColor *)[notificationCenter valueForKey:@"object"];
    [viewBg setBackgroundColor:color];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    return [textField resignFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([string isEqualToString:@""]) {
        if ([textField.text length] > 0) {
            
            NSString *text = [textField.text substringToIndex:[textField.text length] - 1];
            [lblWriteHere setText:text];
        }
    }
    else {
        [lblWriteHere setText:[textField.text stringByAppendingString:string]];
    }
    return YES;
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@"\n"]) {
        textView.text = [textView.text stringByAppendingString:text];
        [textView resignFirstResponder];
    }
    
    return YES;
}
- (void)textViewDidChange:(UITextView *)textView {
    
    [lblWriteHere setHidden:textView.text.length];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
