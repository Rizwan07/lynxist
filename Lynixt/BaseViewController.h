//
//  BaseViewController.h
//  Lynixt
//
//  Created by Rizwan on 7/21/14.
//  Copyright (c) 2014 com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"

@interface BaseViewController : UIViewController

-(BOOL)emailFormat:(NSString *)emailText;
- (void)shakeView:(UIView *)view;

@end
